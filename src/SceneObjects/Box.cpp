#include <cmath>
#include <assert.h>

#include "Box.h"

bool Box::intersectLocal( const ray& r, isect& i ) const
{
	vec3f p = r.getPosition();
	vec3f d = r.getDirection();
	/*
	if (d[0] + d[1] == 0.5
		|| d[0] + d[1] == -0.5
		|| d[0] + d[2] == 0.5
		|| d[0] + d[2] == -0.5
		|| d[1] + d[2] == 0.5
		|| d[1] + d[2] == -0.5) {
		return false;
	}
	*/
	float tmin = (-0.5 - p[0]) / d[0];
	float tmax = (0.5 - p[0]) / d[0];
	if (tmin > tmax) swap(tmin, tmax);
	float tymin = (-0.5 - p[1]) / d[1];
	float tymax = (0.5 - p[1]) / d[1];
	if (tymin > tymax) swap(tymin, tymax);
	if ((tmin > tymax) || (tymin > tmax))
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;
	float tzmin = (-0.5 - p[2]) / d[2];
	float tzmax = (0.5 - p[2]) / d[2];
	if (tzmin > tzmax) swap(tzmin, tzmax);
	if ((tmin > tzmax) || (tzmin > tmax))
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	
	if (tmax <= RAY_EPSILON) {
		return false;
	}

	i.obj = this;
	i.t = tmin > RAY_EPSILON ? tmin : tmax;
	vec3f ipt = r.at(i.t); // intersection point
	if (abs(ipt[0] + 0.5) < RAY_EPSILON) {
		i.N = vec3f(-1, 0, 0);
	}
	else if (abs(ipt[0] - 0.5) < RAY_EPSILON) {
		i.N = vec3f(1, 0, 0);
	}
	else if (abs(ipt[1] + 0.5) < RAY_EPSILON) {
		i.N = vec3f(0, -1, 0);
	}
	else if (abs(ipt[1] - 0.5) < RAY_EPSILON) {
		i.N = vec3f(0, 1, 0);
	}
	else if (abs(ipt[2] + 0.5) < RAY_EPSILON) {
		i.N = vec3f(0, 0, -1);
	}
	else if (abs(ipt[2] - 0.5) < RAY_EPSILON) {
		i.N = vec3f(0, 0, 1);
	}
	else {
		//printf("error in calulating normal in Box.cpp");
		return false;
	}
	/*
	if (abs(tmin - tymin) < RAY_EPSILON) {
		if (d[1] > 0.0) {
			i.N = vec3f(0.0, -1.0, 0.0);
		}
		else {
			i.N = vec3f(0.0, 1.0, 0.0);
		}
	}
	else if (abs(tmin - tzmin) < RAY_EPSILON) {
		if (d[2] > 0.0) {
			i.N = vec3f(0.0, 0.0, -1.0);
		}
		else {
			i.N = vec3f(0.0, 0.0, 1.0);
		}
	}
	else {
		if (d[0] > 0.0) {
			i.N = vec3f(-1.0, 0.0, 0.0);
		}
		else {
			i.N = vec3f(1.0, 0.0, 0.0);
		}
	}
	*/

	return true;
}
