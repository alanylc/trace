#include "ray.h"
#include "material.h"
#include "light.h"
#include "scene.h"
#include "../ui/TraceUI.h"
extern TraceUI* traceUI;

// Apply the phong model to this point on the surface of the object, returning
// the color of that point.
vec3f Material::shade( Scene *scene, const ray& r, const isect& i ) const
{
	// For now, this method just returns the diffuse color of the object.
	// This gives a single matte color for every distinct surface in the
	// scene, and that's it.  Simple, but enough to get you started.
	// (It's also inconsistent with the phong model...)

	// Your mission is to fill in this method with the rest of the phong
	// shading model, including the contributions of all the light sources.
    // You will need to call both distanceAttenuation() and shadowAttenuation()
    // somewhere in your code in order to compute shadows and light falloff.

	// return kd;

	// YOUR CODE HERE
	
	vec3f one_minus_kt = vec3f(1, 1, 1) - kt;
	vec3f V = -r.getDirection();
	
	vec3f I = ke;
	
	for (Scene::cliter it = scene->beginLights(); it != scene->endLights(); it++) {
		Light* light = *it;
		double distance_atten = light->distanceAttenuation(r.at(i.t));
		vec3f shadow_atten = light->shadowAttenuation(r.at(i.t));
		vec3f light_color = light->getColor(r.at(i.t));
		vec3f L = light->getDirection(r.at(i.t)); // light direction
		vec3f R = (-L + (2 * L * i.N) * i.N).normalize(); // reflected direction
		
		double VdotR = max(0.0, V * R);
		double NdotL = max(0.0, i.N * L);
		double ns = shininess * 128;
		

		if (light->getType()=="ambient") {
			I += ka % light_color % one_minus_kt;
		}
		else {
			I += (distance_atten * shadow_atten) % light_color
				% (kd * NdotL % one_minus_kt + ks * pow(VdotR, ns));
		}
	}
	I *= traceUI->getIntensity();
	
	return I;
}
