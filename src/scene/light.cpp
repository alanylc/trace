#include <cmath>

#include "light.h"
#include "../ui/TraceUI.h"
extern TraceUI* traceUI;

double DirectionalLight::distanceAttenuation( const vec3f& P ) const
{
	// distance to light is infinite, so f(di) goes to 0.  Return 1.
	return 1.0;
}


vec3f DirectionalLight::shadowAttenuation(const vec3f& P) const
{
	// YOUR CODE HERE:
	// You should implement shadow-handling code here.
	// return vec3f(1,1,1);

	vec3f d = -orientation;
	ray r(P, d);
	isect i;
	if (scene->intersect(r, i) && i.t > RAY_EPSILON) {
		return i.getMaterial().kt % shadowAttenuation(r.at(i.t));
	}
	else {
		return vec3f(1, 1, 1);
	}
	
	/*if (scene->intersect(r, i)) {
		if (!i.getMaterial().kt.iszero()) {
			return i.getMaterial().kt;
		}
		else if (!i.getMaterial().ka.iszero()) {
			return vec3f(1, 1, 1);
		}
		else {
			return vec3f(0, 0, 0);
		}
	}
	else {
		return vec3f(1, 1, 1);
	}*/

	/*if (scene->intersect(r, i) && i.t > RAY_EPSILON) {
		if (!i.getMaterial().kt.iszero()) {
			return i.getMaterial().kt % shadowAttenuation(r.at(i.t));
		}
		else if (!i.getMaterial().ka.iszero()) {
			return vec3f(1, 1, 1);
		}
		else {
			return vec3f(0, 0, 0);
		}
	}
	else {
		return vec3f(1, 1, 1);
	}*/
	
}

vec3f DirectionalLight::getColor( const vec3f& P ) const
{
	// Color doesn't depend on P 
	return color;
}

vec3f DirectionalLight::getDirection( const vec3f& P ) const
{
	return -orientation;
}

double PointLight::distanceAttenuation( const vec3f& P ) const
{
	// YOUR CODE HERE

	float a = traceUI->getConstant();
	float b = traceUI->getLinear();
	float c = traceUI->getQuadratic();
	if (constant_attenuation_coeff >= 0.0)	a = constant_attenuation_coeff;
	if (linear_attenuation_coeff >= 0.0)	b = linear_attenuation_coeff;
	if (quadratic_attenuation_coeff >= 0.0)	c = quadratic_attenuation_coeff;
	float d = vec3f(position - P).length() * traceUI->getDistance();
	float v = 1 / (a + b*d + c*d*d);
	return min(1.0, v);

	// You'll need to modify this method to attenuate the intensity 
	// of the light based on the distance between the source and the 
	// point P.  For now, I assume no attenuation and just return 1.0

	// return 1.0;
}

vec3f PointLight::getColor( const vec3f& P ) const
{
	// Color doesn't depend on P 
	return color;
}

vec3f PointLight::getDirection( const vec3f& P ) const
{
	return (position - P).normalize();
}


vec3f PointLight::shadowAttenuation(const vec3f& P) const
{
    // YOUR CODE HERE:

	// You should implement shadow-handling code here.
	// return vec3f(1, 1, 1);

	isect i;
	vec3f d = position - P;
	ray r(P, d.normalize());
	if (scene->intersect(r, i)) {
		if (RAY_EPSILON < i.t && i.t < d.length()) {
			return i.getMaterial().kt % shadowAttenuation(r.at(i.t));
		}
		else return vec3f(1, 1, 1);
	}
	else return vec3f(1, 1, 1);
}

double AmbientLight::distanceAttenuation(const vec3f& P) const
{
	return 1.0;
}


vec3f AmbientLight::shadowAttenuation(const vec3f& P) const
{
	return vec3f(1, 1, 1);
}

vec3f AmbientLight::getColor(const vec3f& P) const
{
	// Color doesn't depend on P 
	return color;
}

vec3f AmbientLight::getDirection(const vec3f& P) const
{
	return vec3f(0, 0, 0);
}