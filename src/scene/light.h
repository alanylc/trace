#ifndef __LIGHT_H__
#define __LIGHT_H__

#include "scene.h"

class Light
	: public SceneElement
{
public:
	virtual vec3f shadowAttenuation(const vec3f& P) const = 0;
	virtual double distanceAttenuation( const vec3f& P ) const = 0;
	virtual vec3f getColor( const vec3f& P ) const = 0;
	virtual vec3f getDirection( const vec3f& P ) const = 0;
	inline string getType() { return type; };

protected:
	Light( string _type, Scene *scene, const vec3f& col )
		: SceneElement( scene ), color( col ), type(_type) {}

	vec3f 		color;
	string		type;
};

class DirectionalLight
	: public Light
{
public:
	DirectionalLight( Scene *scene, const vec3f& orien, const vec3f& color )
		: Light( "directional", scene, color ), orientation( orien ) {}
	virtual vec3f shadowAttenuation(const vec3f& P) const;
	virtual double distanceAttenuation( const vec3f& P ) const;
	virtual vec3f getColor( const vec3f& P ) const;
	virtual vec3f getDirection( const vec3f& P ) const;

protected:
	vec3f 		orientation;
};

class PointLight
	: public Light
{
public:
	PointLight(Scene *scene, const vec3f& pos, const vec3f& color, double _constant_attenuation_coeff, double _linear_attenuation_coeff, double _quadratic_attenuation_coeff )
		: Light("point", scene, color), 
		position(pos), 
		constant_attenuation_coeff(_constant_attenuation_coeff), 
		linear_attenuation_coeff(_linear_attenuation_coeff), 
		quadratic_attenuation_coeff(_quadratic_attenuation_coeff) {}
	virtual vec3f shadowAttenuation(const vec3f& P) const;
	virtual double distanceAttenuation( const vec3f& P ) const;
	virtual vec3f getColor( const vec3f& P ) const;
	virtual vec3f getDirection( const vec3f& P ) const;

protected:
	vec3f position;
	double constant_attenuation_coeff, linear_attenuation_coeff, quadratic_attenuation_coeff;
};

class AmbientLight
	: public Light
{
public:
	AmbientLight(Scene *scene, const vec3f& color)
		: Light("ambient", scene, color) {}
	virtual vec3f shadowAttenuation(const vec3f& P) const;
	virtual double distanceAttenuation(const vec3f& P) const;
	virtual vec3f getColor(const vec3f& P) const;
	virtual vec3f getDirection(const vec3f& P) const;
};

#endif // __LIGHT_H__
